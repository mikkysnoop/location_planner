
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/materia/bootstrap.min.css" integrity="sha384-B4morbeopVCSpzeC1c4nyV0d0cqvlSAfyXVfrPJa25im5p+yEN/YmhlgQP/OyMZD" crossorigin="anonymous">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link href="css/addons-pro/steppers.css" rel="stylesheet">
    <!-- Stepper CSS - minified-->
    <link href="css/addons-pro/steppers.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Stepper JavaScript -->
    <script type="text/javascript" src="js/addons-pro/steppers.js"></script>
    <!-- Stepper JavaScript - minified -->
    <script type="text/javascript" src="js/addons-pro/steppers.min.js"></script>
</head>


<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Location Planner</a>
        </div>
        {{--        <ul class="nav navbar-nav">--}}
        {{--            <li class="active"><a href="#">Home</a></li>--}}
        {{--            <li><a href="#">Page 1</a></li>--}}
        {{--            <li><a href="#">Page 2</a></li>--}}
        {{--            <li><a href="#">Page 3</a></li>--}}
        {{--        </ul>--}}
    </div>
</nav>
<div class="container">
        <br/>

      <form  method="post" action="{{route('import.content')}} " enctype="multipart/form-data">
          @csrf
        <div class="col-sm-4">
            <input name="uploaded_file" type="file" class="form-control" />
        </div>
        <div class="col-sm-4">
            <button class="form-control btn-info">Upload Excel File</button>
        </div>

        <div class="col-sm-4">
            <select id="neighbour" class="form-control">
                <option value="null">Select Neighbourhood</option>
                @foreach($neighbourhood as $neighbourhoods)
                    @if($neighbourhoods->neighbourhood != 'Neighbourhood of Required Service Delivery')
                    <option @if($location == $neighbourhoods->neighbourhood) selected @endif value="{{str_replace('&', '-', $neighbourhoods->neighbourhood)}}">{{$neighbourhoods->neighbourhood}}</option>
                    @endif
                @endforeach
            </select>
        </div>
      </form>

    <br/> <br/>  <br/>

    <div class="col-sm-6">
         Driving
        <input @if($mode == 1) checked @endif onclick="fetchData(1)" type="checkbox"/>  &nbsp; &nbsp;
        Transit
        <input @if($mode == 2) checked @endif onclick="fetchData(2)" type="checkbox"/>  &nbsp; &nbsp;
        Walking
        <input @if($mode == 3) checked @endif onclick="fetchData(3)" type="checkbox"/>  &nbsp; &nbsp;
        Bicycling
        <input @if($mode == 4) checked @endif onclick="fetchData(4)" type="checkbox"/>
    </div>

    <br/>  <br/>

    <table  class="table table-bordered table-hover">
        <thead class="bg-info">
        <tr style="font-size: 13px">
            <th>Traveling</th>
            <th>Postcode</th>
            <th>Neighbourhood</th>
            <th>Am</th>
            <th>Lunch</th>
            <th>Tea</th>
            <th>PM</th>
            <th>Other</th>
            <th nowrap>Weekly Hours</th>
        </tr>
        </thead>
        <tbody>
        @foreach($record as $key => $records)
        <tr style="font-size: 10px">
            <td nowrap>@if($key == 0) <b>Starting Point</b>  @else Location {{$key}} @endif</td>
            <td nowrap>{{$records->post_code_of_required_service_delivery}} &nbsp;&nbsp; <input type="checkbox"/></td>
            <td nowrap>{{$records->neighbourhood}}</td>
            <td nowrap>{{$records->am}}</td>
            <td nowrap>{{$records->lunch}}</td>
            <td nowrap>{{$records->tea}}</td>
            <td nowrap>{{$records->pm}}</td>
            <td nowrap>{{$records->Other}}</td>
            <td nowrap>{{$records->total_weekly_hours}}</td>
        </tr>


        @if($mode == 1)
            <tr class="bg-warning" style="font-size: 10px">
                @if($key + 1 != $max)
                    <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp; <i class="fas fa-car-side"></i> &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <i>
                            <?php $duration =  \App\Http\Controllers\Process::getDistanceByLand($records->post_code_of_required_service_delivery,$record[$key + 1]->post_code_of_required_service_delivery, 1);
                            echo $duration;
                            ?>
                        </i>
                    </td>
                @endif
            </tr>
        @endif
        @if($mode == 2)
            <tr class="bg-warning" style="font-size: 10px">
                @if($key + 1 != $max)
                    <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp; <i class="fas fa-bus-alt"></i> &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <i>
                            <?php $duration =  \App\Http\Controllers\Process::getDistanceByLand($records->post_code_of_required_service_delivery,$record[$key + 1]->post_code_of_required_service_delivery, 2);
                            echo $duration;
                            ?>
                        </i>
                    </td>
                @endif
            </tr>
        @endif
        @if($mode == 3)
            <tr class="bg-warning" style="font-size: 10px">
                @if($key + 1 != $max)
                    <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa-solid fa-person-walking"></i> &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <i>
                            <?php $duration =  \App\Http\Controllers\Process::getDistanceByLand($records->post_code_of_required_service_delivery,$record[$key + 1]->post_code_of_required_service_delivery, 3);
                            echo $duration;
                            ?>
                        </i>
                    </td>
                @endif
            </tr>
        @endif
        @if($mode == 4)
            <tr class="bg-warning" style="font-size: 10px">
                @if($key + 1 != $max)
                    <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa-solid fa-bicycle"></i></i> &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <i>
                            <?php $duration =  \App\Http\Controllers\Process::getDistanceByLand($records->post_code_of_required_service_delivery,$record[$key + 1]->post_code_of_required_service_delivery, 4);
                            echo $duration;
                            ?>
                        </i>
                    </td>
                @endif
            </tr>
        @endif
        @endforeach
        </tbody>
    </table>

</div>
</html>




<script>
    $(document).ready(function(){
        $('#neighbour').change(function(){
            window.location.href = '/process?location=' + $(this).val();
        });
    });

    function fetchData(mode){
        window.location.href = '{{$requestSearch}}&mode='+mode;
    }
</script>
