<?php

namespace App\Http\Controllers;

use App\Models\Documents;
use App\Models\Records;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Player;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;
use JustSteveKing\LaravelPostcodes\Facades\Postcode;

class ImportData extends Controller
{
    public function uploadContent(Request $request)
    {
       // dd($request->all());
        $file = $request->file('uploaded_file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension(); //Get extension of uploaded file
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize(); //Get size of uploaded file in bytes
//Check for file extension and size
            $this->checkUploadedFileProperties($extension, $fileSize);
//Where uploaded file will be stored on the server
            $location = 'uploads'; //Created an "uploads" folder for that
// Upload file
            $file->move($location, $filename);
// In case the uploaded file path is to be stored in the database
            $filepath = public_path($location . "/" . $filename);
// Reading file
            $file = fopen($filepath, "r");
            $importData_arr = array(); // Read through the file and store the contents as an array
            $i = 0;
//Read the contents of the uploaded file
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata);
// Skip first row (Remove below comment if you want to skip the first row)
                if ($i == 0) {
                    $i++;
                    continue;
                }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            fclose($file); //Close after reading
            $j = 0;

            Documents::truncate();

            foreach ($importData_arr as $importData) {
                $j++;
                try {
                    $records = new Documents();
                    $records->Client_forename = $importData[1];
                    $records->client_surname = $importData[2];
                    $records->start_date = $importData[3];
                    $records->Where_was_the_client_when_carefinding_started = $importData[4];
                    $records->Current_circumstances = $importData[5];
                    $records->Brokerage_assistant = $importData[6];
                    $records->care_coordniator = $importData[7];
                    $records->post_code_of_required_service_delivery = $importData[8];
                    $records->neighbourhood = $importData[9];
                    $records->required_service_type = $importData[10];
                    $records->am = $importData[11];
                    $records->lunch = $importData[12];
                    $records->tea = $importData[13];
                    $records->pm = $importData[14];
                    $records->other = $importData[15];
                    $records->total_weekly_hours = $importData[16];
                    $records->decimal = $importData[17];
                    $records->n_hood_provider = $importData[18];
                    $records->latitude = 0; //$this->getLatitude($importData[8]);
                    $records->longitude =0; // $this->getLongitude($importData[8]);
                    $records->save();
//Send Email
                   // $this->sendEmail($email, $name);
                //    DB::commit();
                } catch (\Exception $e) {
//throw $th;
                //    DB::rollBack();
                }
            }
            return redirect()->back();
        } else {
//no file was uploaded
            return redirect()->back();
            //throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }
    }

    private function getLatitude($postcode){
        $data = Postcode::getPostcode($postcode);
        return $data->latitude;
    }

    private function getLongitude($postcode){
        $data = Postcode::getPostcode($postcode);
        return $data->longitude;
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = array("csv", "xlsx"); //Only want csv and excel files
        $maxFileSize = 2097152; // Uploaded file size limit is 2mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }

//    public function sendEmail($email, $name)
//    {
//        $data = array(
//            'email' => $email,
//            'name' => $name,
//            'subject' => 'Welcome Message',
//        );
//        Mail::send('welcomeEmail', $data, function ($message) use ($data) {
//            $message->from('welcome@myapp.com');
//            $message->to($data['email']);
//            $message->subject($data['subject']);
//        });
//    }
}
