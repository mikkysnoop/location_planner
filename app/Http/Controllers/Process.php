<?php

namespace App\Http\Controllers;

use App\Models\Documents;
use App\Models\Records;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use JustSteveKing\LaravelPostcodes\Facades\Postcode;
use JustSteveKing\LaravelPostcodes\Service\PostcodeService;
use TeamPickr\DistanceMatrix\Frameworks\Laravel\DistanceMatrix;
use TeamPickr\DistanceMatrix\Licenses\StandardLicense;
use Illuminate\Support\Facades\Http;

class Process extends Controller
{
    protected $postcodes;

    public function __construct(PostcodeService $service)
    {
        $this->postcodes = $service;
    }

    public function index(Request $request)
    {
        $search = str_replace('-', '&', $request->get('location'));
        $neighbourhood = Documents::selectRaw('id, neighbourhood')->groupBy('neighbourhood')->get();
        if ($request->get('location')) {
            $record = Documents::where('neighbourhood', $search)->orderBy('post_code_of_required_service_delivery')->get();
            $max = count($record);
        }

        $requestSearch = $request->url() . '?location=' . $request->get('location');

        return View('process')->with('neighbourhood', $neighbourhood)
            ->with('record', $record ?? [])
            ->with('max', $max ?? null)
            ->with('requestSearch', $requestSearch)
            ->with('mode', $request->get('mode'))
            ->with('location', $search);
    }

    public static function distanceByPlane($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "M") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    //returns in meters
    public static function getDistanceByLand($post1, $post2, $mode)
    {
       try {
           if ($mode == 1) {
               $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations=' . $post1 . '&origins=' . $post2 . '&key=' . env('GOOGLE_MAPS_KEY') . '-8zc3R_X2C1yUw&mode=driving');
               $distance = $response->object()->rows[0]->elements[0]->distance->text;
               $duration = $response->object()->rows[0]->elements[0]->duration->text;
               return '(' . $distance . ') ' . $duration;
           } elseif ($mode == 2) {
               $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations=' . $post1 . '&origins=' . $post2 . '&key=' . env('GOOGLE_MAPS_KEY') . '-8zc3R_X2C1yUw&mode=transit');
               $distance = $response->object()->rows[0]->elements[0]->distance->text;
               $duration = $response->object()->rows[0]->elements[0]->duration->text;
               return '(' . $distance . ') ' . $duration;
           } elseif ($mode == 3) {
               $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations=' . $post1 . '&origins=' . $post2 . '&key=' . env('GOOGLE_MAPS_KEY') . '-8zc3R_X2C1yUw&mode=walking');
               $distance = $response->object()->rows[0]->elements[0]->distance->text;
               $duration = $response->object()->rows[0]->elements[0]->duration->text;
               return '(' . $distance . ') ' . $duration;
           } elseif ($mode == 4) {
               $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations=' . $post1 . '&origins=' . $post2 . '&key=' . env('GOOGLE_MAPS_KEY') . '-8zc3R_X2C1yUw&mode=bicycling');
               $distance = $response->object()->rows[0]->elements[0]->distance->text;
               $duration = $response->object()->rows[0]->elements[0]->duration->text;
               return '(' . $distance . ') ' . $duration;
           }
       } catch (\Exception $se){
           echo 'Error Something went Wrong';
       }

    }

    public function deployment()
    {
       dd(123);
    }
}
