<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Process;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/upload-content',[\App\Http\Controllers\ImportData::class,'uploadContent'])->name('import.content');
Route::get('/process', [Process::class, 'index']);
